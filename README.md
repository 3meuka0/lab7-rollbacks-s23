# Lab7 - Rollbacks and reliability

## Introduction

Probably you've already worked with reliability at Distributed Systems course. So today we are going to talk about rollback implementation in software.(Though I think you've worked with it as well, but maybe don't know about it) ***Let's roll!***

## Rollbacks

If you have a very complicated or very important function that you need to execute it is a good thing to implement Rollbacks there. You are basically saving the state of the execution, and you are returning to it in case of faliure on the next execution state, to try execute next step once again or return the results of at least previous step. One of the most used areas is databases, when you have a chunk of requests and you want all of them to pass, or none of them, so you are sending them in one commit, and if failure happens, your changes from commit are being removed.

## Lab

Ok, let's crush all the stuff out of it:
1. Create your fork of the `
Lab9 - Rollbacks and reliability
` repo and clone it.
2. We are going to write some code today, our task is to make some backend for in-game shop. Instances that we should manage as follows: players (their names and balance), shop itself (limited only to list of products, their price and quantity in stock) plus for homework your task will be to implement inventory of a player. We will be storing all our data in databse, lets use for example postgresql.
+ Install postgresql(If you have Ubuntu/Mint you should already have it installed)
+ Now, let's open postgresql and create DB like this:
```sql
CREATE DATABASE lab;
\c lab
CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
```
+ Pay attention to constraints that we have for created table. Player balance, price and amount of product in stock cannot be negative. 
+ Ofcourse for testing we need some sample data, lets fill in tables with it:
```
INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT INTO PLayer (username, balance) VALUES ('Bob', 200);

INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10);
```
+ Ok, then you have some code in lab, for todays lab we will use python, but you may use any programming language you like. There is the single function `buy_product`, that does all the staff.

How does it works? First of all in the beginning we have import of library that works with postgresql. A bit bellow there is connection code to connect to database and templates of our queries to database. psycopg2 automatically creates, finishes and rollbacks transactions.

There is also `try-except` blocks insyde. Actually there is two ways of checking that there is no such player or balance is to low: you may get this data using multiple select queries (`SELECT balance FROM Player WHERE username = %s` and `SELECT price FROM Shop WHERE product = %s`) and check it in code, or you may rely on checks in database (if constraints that we created for tables failed - psycopg2 raises `CheckViolation` exception and we may handle it) as shown in example below and check count of updated rows. 

## Homework

### Part 1 (2/3 points)
If you try to run given function, you can see that it leads to data inconsistency. As a first part of homework, you should fix this issue(hint, it is better to use 'with' than not to use)

### Part 2 (3/3 points)
When given function work without leading to inconsistency, you should extend its functionality and add increasing amount of product in player inventory.
To make this you will need to add table `Inventory` which stores username, product name and amount of this product in inventory. 

There is also constraint that in inventory player can store only 100 products in total (e.g. if player have 20 marshmellows and 10 lets say bananas, then there is still possible to add 70 items). If player reached limit of items in inventory, than buy transaction should be rolled back and appropriate exception raised. 

P.S. No PL limitation in this lab, you should use Postgres (or any other relational DBMS with transactions support), but you may implement rollbacks in any PL you wish.
P.S.2 This lab is write only, so I would not run and test your code(until I have no doubts), mostly I am going oly to read your code, so you should show the understanding of concept not to make just working code

### Extra part (1.5 points)
I believe that first part was easy for you. It requires to know some industry standards. But what if everything is commited but function still fail(e.g. electricity shortage, client-server connection broken or something else). In this case some clients might try to retry the request.
What would happen then? What is your solution for this scenario?

### Extra part (1.5 points)
Ok, working with single database is pretty simple, what if you have 2 or more storages? It might be not only databases, but external REST CRUD services or message queues.
And it is possible that one of that external storage have no option to rollback or made revers changes.

What is possible approaches here?

As an example you might refer to next:
```python
result = decrease_balance_in_db()
message_queue.send({"task": "start_money_withdrawal", "user_id": user_id, "amount": amount})
```
So in this case, due to long real money withdrawal, this operation would be made in background.
And that might be a case, that amount of money in database is decreased, but real money is not changed.

What are possible approaches here?
Let's imagine that you can not merge that into single operation

P.S.3 If you would do extra tasks, I consider just text answers with examples of code. But it is really hard to find changes if you are changing this file(REAMDE.md). So, please, add new files with solutions.


## Solution

### Part 1 (2/3 points)

```
CREATE DATABASE lab;
CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT INTO Player (username, balance) VALUES ('Bob', 200);
INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10);
```

### Part 2 (3/3 points)

```
CREATE TABLE Inventory (
    username TEXT REFERENCES Player(username) NOT NULL,
    product  TEXT REFERENCES Shop(product) NOT NULL,
    amount   INT CHECK (amount >= 0),
    UNIQUE(username, product)
);

CREATE OR REPLACE FUNCTION check_inventory_limit_size() RETURNS TRIGGER AS $$
DECLARE
    total_sum_amount INT;
BEGIN
    SELECT SUM(amount) INTO total_sum_amount FROM Inventory WHERE username = NEW.username;
    IF total_sum_amount > 100 THEN
        RAISE EXCEPTION 'Cannot place more than 100 items in user''s inventory';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
END;

CREATE TRIGGER inventory_limit BEFORE INSERT ON inventory FOR EACH ROW EXECUTE FUNCTION check_inventory_limit_size();
```
